package com.example.demo.controller;

import com.example.demo.DemoApplicationTest;
import com.example.demo.dto.GreetingDTO;
import org.apache.http.entity.ContentType;
import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GreetingControllerIT extends DemoApplicationTest{
    private static final String CONTENT = "Aloha, World! Glad to see you here :)))";
    private static final String CREATE_GREETING_ENDPOINT = "/greeting";


    @Test
    public void when_calling_create_greetingDTO_endpoint_expect_body_and_status_ok() throws Exception{
        GreetingDTO greetingDTO = prepareTestDto();

        MvcResult mvcResult = this.mockMvc.perform(
                post(CREATE_GREETING_ENDPOINT, GreetingDTO.class)    //Правильно ли?
                    .contentType(ContentType.APPLICATION_JSON.getMimeType())
                    .content(objectMapper.writeValueAsString(greetingDTO)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        String response = mvcResult.getResponse()
                .getContentAsString();
        GreetingDTO greetingDTOResult = objectMapper.readValue(response, GreetingDTO.class);

        assertNotNull(greetingDTOResult);
        assertEquals(greetingDTO, greetingDTOResult);
    }

    private GreetingDTO prepareTestDto(){
        GreetingDTO greetingDTO = new GreetingDTO();
        greetingDTO.setContent(CONTENT);
        greetingDTO.setId(1L);

        return greetingDTO;
    }
}
