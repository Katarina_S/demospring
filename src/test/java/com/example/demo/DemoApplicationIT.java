package com.example.demo;

import com.example.demo.dto.GreetingDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.entity.ContentType;
import org.hibernate.validator.internal.util.Contracts;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class DemoApplicationTest {

    private static final String URL_GREETING = "/greeting";


    @Autowired
    private WebApplicationContext webApplicationContext;

    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

//    @Test
//    public void when_calling_create_greetingDTO_endpoint_expect_body_and_status_ok() throws Exception{
//        GreetingDTO greetingDTO = prepareTestDto();
//
//        MvcResult mvcResult = this.mockMvc.perform(
//                post(URL_GREETING, GreetingDTO.class)    //Правильно ли?
//                        .contentType(ContentType.APPLICATION_JSON.getMimeType())
//                        .content(objectMapper.writeValueAsString(greetingDTO)))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andReturn();
//
//        String response = mvcResult.getResponse()
//                .getContentAsString();
//        GreetingDTO greetingDTOResult = objectMapper.readValue(response, GreetingDTO.class);
//
//        Contracts.assertNotNull(greetingDTOResult);
//        assertEquals(greetingDTO, greetingDTOResult);
//    }


	@Test
	public void contextLoads() throws Exception {

        GreetingDTO greetingDTOReal = prepareTestDto();

        MvcResult mvcResult = mockMvc.perform(
                get(URL_GREETING)
                    .content(objectMapper.writeValueAsString(greetingDTOReal)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        String response = mvcResult.getResponse()
                .getContentAsString();

        GreetingDTO greetingDTO = objectMapper.readValue(response, GreetingDTO.class);
        assertNotNull(greetingDTO);
        assertEquals(greetingDTOReal, greetingDTO);

	}

    private GreetingDTO prepareTestDto() {
        GreetingDTO greetingDTO = new GreetingDTO();
        greetingDTO.setContent("Aloha, World! Glad to see you here :)))");
        greetingDTO.setId(1L);
        return greetingDTO;
    }

    @PostConstruct
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();
    }
}
