package com.example.controller;

import com.example.dto.GreetingDTO;
import com.example.service.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class GreetingController {

    private static final String template = "Aloha, %s! Glad to see you here :)))";
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    private GreetingService greetingService;

    @RequestMapping("/greeting")
    @GetMapping
    public GreetingDTO greeting(@RequestParam(value="name", defaultValue="World") String name) {
        GreetingDTO greetingDTO = new GreetingDTO();
        greetingDTO.setContent(String.format(template, name));
        greetingDTO.setId(counter.incrementAndGet());
        return createGreeting(greetingDTO);
    }

    @PostMapping(path = "/greeting")
    public GreetingDTO createGreeting(@RequestBody GreetingDTO greetingDTO){
        return greetingService.createGreeting(greetingDTO);
    }
}
