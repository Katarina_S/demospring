package com.example.model;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;

@Entity
@Table(name = "Greeting")
@EntityScan
public class Greeting {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "content")
    private String content;

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    private Boolean deleted = Boolean.FALSE;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Greeting{" +
                "id=" + id +
                ", content='" + content +
                '}';
    }
    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;

        Greeting greeting = (Greeting) o;

        return content != null ? content.equals(greeting.content) : greeting.content == null;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        return result;
    }
}
