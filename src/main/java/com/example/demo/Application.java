package com.example;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.sql.Connection;
import java.sql.DriverManager;


@SpringBootApplication
public class Application{

	public static void main(String[] args)  throws Exception {
		Class.forName("org.h2.Driver");
		Connection conn = DriverManager.
				getConnection("jdbc:h2:mem:dataSource;Mode=MySql;DB_CLOSE_ON_EXIT=FALSE;", "sa", "");
		SpringApplication.run(Application.class, args);
		conn.close();
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
