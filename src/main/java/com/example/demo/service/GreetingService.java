package com.example.service;

import com.example.model.Greeting;
import com.example.dto.GreetingDTO;
import com.example.repository.GreetingRepository;
import com.example.util.ConvertHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.Transactional;

@Transactional
@Service
@Primary
@Profile("service")
public class GreetingService {

    private EntityManagerFactory entityManagerFactory;

    @PersistenceUnit
    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory){
        this.entityManagerFactory = entityManagerFactory;
    }

    public Greeting getById(Long id){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        return entityManager.find(Greeting.class, id);
    }

    @Autowired
    private ConvertHelper convertHelper;

    @Autowired
    private GreetingRepository greetingRepository;


    public GreetingDTO createGreeting(GreetingDTO greetingDTO){
        Greeting greeting = new Greeting();

        storeGreeting(greeting, greetingDTO);
        return convertHelper.convertFrom(greeting, GreetingDTO.class);
    }

    private void storeGreeting(Greeting greeting, GreetingDTO greetingDTO){
        greeting.setContent(greetingDTO.getContent());
        greeting.setId(greetingDTO.getId());
        greetingRepository.save(greeting);
    }
}

