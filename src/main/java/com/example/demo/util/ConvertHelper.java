package com.example.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;

@Component
public class ConvertHelper {

    @Autowired
    private ModelMapper modelMapper;

    public <From, To> To convertFrom(From src, Class<To> convertTo) {
        return convertTo.cast(modelMapper.map(src, convertTo));
    }
}
